import { BadRequestError, NotFoundError } from 'routing-controllers';
import { FlightsModel } from '../models/flights.model'
import { PersonsModel } from '../models/persons.model';

export class FlightsService {
    getAll() {
        return FlightsModel.find()
    }

    async onboarding(p: {flightId: string, personId: string }) {
        const { flightId, personId } = p;

        const flight = await FlightsModel.findById(flightId);
        if (!flight) {
            throw new NotFoundError('Flight not found');
        }

        const person = await PersonsModel.findById(personId);
        if (!person) {
            throw new NotFoundError('Person not found');
        }

        console.log('Flight ====>', flight);

        flight.passengers.push(personId);

        return FlightsModel.findByIdAndUpdate(flightId, flight, { new: true });
    }
}
