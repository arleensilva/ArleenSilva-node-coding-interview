import { JsonController, Get, Post, Body } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    async getAll() {
        const data = await flightsService.getAll();
        return {
            status: 200,
            data,
        }
    }

    @Post('/onboarding', { transformResponse: false })
    async onboarding(@Body() p: any) {

        console.log(`Onboarding person ${p.personId} on fligh ${p.flightId}`);

        const data = await flightsService.onboarding(p);
        return {
            status: 200,
            data,
        }
    }
}
